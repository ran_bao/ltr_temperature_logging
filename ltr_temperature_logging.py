# -*- coding: utf-8 -*-
import logging
import time
import datetime
from pcu_test.scripts.pcu_data_logger import DBDictWriter
from enphase_equipment.data_logger.agilent import Agilent34970A
from collections import OrderedDict
import mysql.connector


datalogger_channels = OrderedDict([
    ('101', 'Temperature1'),
    ('102', 'Temperature2'),
    ('103', 'Temperature3')
])


class LTRTempWriter(DBDictWriter):
    def __init__(self, *args, **kwargs):
        super(LTRTempWriter, self).__init__(*args, **kwargs)
        self.log = logging.getLogger(self.__class__.__name__)

    def create_table(self, tablename, data):
        raise NotImplementedError('Always assume database exists')

    def _save_to_db(self, data, tablename):
        def transaction():
            if not self.db_conn.is_connected():
                self.db_conn.reconnect()

            # Get cursor
            cursor = self.db_conn.cursor()

            # Assemble query
            query = '''INSERT INTO {} (DateTime, Temperature1, Temperature2, Temperature3)
                       VALUES ('{}', '{}', '{}', '{}');'''.format(tablename,
                                                           data['DateTime'],
                                                           data['Temperature1'],
                                                           data['Temperature2'],
                                                           data['Temperature3'])
            self.log.debug(query)

            # Execute query
            cursor.execute(query)
            self.db_conn.commit()
            self.db_conn.close()

        self._do_transaction(transaction)

    def _do_transaction(self, transaction_callable, *args, **kwargs):
        retry_cnt = 0
        while retry_cnt < 10:
            self._check_for_abort()
            retry_cnt += 1
            try:
                transaction_callable(*args, **kwargs)
            except Exception as e:
                self.log.warning('Unable to run transaction: {}\n'
                                 'Depth of the queue: {}\n'
                                 'Retry: {}'.format(e, self._transaction_queue.qsize(), retry_cnt))
                time.sleep(10)
            else:
                break
        else:
            reason = 'Unable to write to database after {} trials'.format(retry_cnt)
            self.log.error(reason)
            raise RuntimeError(reason)

        self.log.info('Transaction successful')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    # Open database connection
    ctx = mysql.connector.connect(host='10.18.24.100',
                                  user='root',
                                  password='rootakk6',
                                  database='encharge',
                                  port=3306)
    ctx.close()
    print('Database connected')

    # Open driver to write to database
    writer = LTRTempWriter(ctx)
    writer.run()

    try:
        datalogger = Agilent34970A('GPIB0::9::INSTR')
        print('Data logger initialized')

        channels = ','.join(list(datalogger_channels.keys()))

        # Setup data loggers
        datalogger.reset()

        # Setup T type transducer
        datalogger.write('CONF:TEMP TC,T,(@{})'.format(channels))

        # Log data
        while True:
            # Response will look like this: '+2.59850000E+01,+2.47890000E+01,+2.49290000E+01'
            response = datalogger.ask('MEAS:TEMP? tc,(@{})'.format(channels))
            capture_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

            temperature_list = map(float, response.split(','))

            results = dict(zip(list(datalogger_channels.values()), temperature_list))
            results['DateTime'] = capture_time

            writer.save_to_db(results, 'ChamberTemperature')

            time.sleep(5)

        # Some dummy debug code:
        # while True:
        #     results = {'DateTime': datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
        #                'Temperature1': 100,
        #                'Temperature2': 200,
        #                'Temperature3': 400, }
        #     writer.save_to_db(results, 'ChamberTemperature')
        #     time.sleep(5)

    finally:
        writer.stop()
        ctx.close()